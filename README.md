# PromiseJS

## 介绍
PromiseJS，一个使高版本浏览器的Promise接口，兼容低版本浏览器下运行

#### 文件大小
|文件名|文件大小|文件说明|
|--|--|--|
|viewx.min.js.zip|0.8k|js代码压缩 + zip压缩，用于网络要求更高的生产运营环境|
|viewx.min.js|1.47k|js代码压缩，用于生产运营时使用|
|viewx.js|3.2k|js源代码，用于开发测试时使用|


#### 兼容浏览器

| 电脑端 | 浏览器 | 最小版本 |
|-|-|-|
| ![Internet Explorer](https://developer.mozilla.org/static/media/internet-explorer.cf17782c.svg "Internet Explorer")| Internet Explorer | 5.5 |
| ![Chrome](https://developer.mozilla.org/static/media/chrome.4c570865.svg "Chrome")| Chrome | 1 |
| ![Edge](https://developer.mozilla.org/static/media/edge.40018f6a.svg "Edge") | Edge  | 12 |
| ![Firefox](https://developer.mozilla.org/static/media/firefox.51d8a59c.svg "Firefox") | Firefox | 3 |
| ![Opera](https://developer.mozilla.org/static/media/opera.a0ab0c50.svg "Opera") | Opera | 15 |
| ![Safari](https://developer.mozilla.org/static/media/safari.3679eb31.svg "Safari") | Safari | 4 |


| 手机端 | 浏览器 | 最小版本 |
|-|-|-|
| ![WebView Android](https://developer.mozilla.org/static/media/android.7d9bf320.svg "WebView Android") | WebView Android | 1 |
| ![Chrome Android](https://developer.mozilla.org/static/media/chrome.4c570865.svg "Chrome Android") | Chrome Android | 18 |
| ![Firefox Android](https://developer.mozilla.org/static/media/firefox.51d8a59c.svg "Firefox Android") | Firefox Android | 4 |
| ![Opera Android](https://developer.mozilla.org/static/media/opera.a0ab0c50.svg "Opera Android") | Opera Android | 14 |
| ![iOS Safari](https://developer.mozilla.org/static/media/safari.3679eb31.svg "iOS Safari") | iOS Safari | 3.2 |
| ![Samsung Internet](https://developer.mozilla.org/static/media/samsung-internet.6fd7f423.svg "Samsung Internet") | Samsung Internet | 1.0 |

## ES6 Promise Bug

在ES6原生Promise中，当Promise的resolve方法执行传入带有then方法的对象时，则Promise的then方法无法执行，示例：

```
new Promise(function(resolve){
	resolve({
		then:function(){ console.log("resolve then"); } //神奇的打印了"resolve then"
	});
}).then(function(result){
	console.log("promise then"); //没有执行打印"promise then"
})
```

> 示例文件：https://gitee.com/silis-open/PromiseJS/blob/master/demo/resolve-then-bug.html